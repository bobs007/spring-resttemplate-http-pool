# spring-resttemplate-http-pool

spring rest template is one of the most popular rest client tools. HTTP connection pooling can improve the performamance; but bit tricky to implement it.


# Reference


https://www.baeldung.com/httpclient-connection-management
https://tech.asimio.net/2016/12/27/Troubleshooting-Spring-RestTemplate-Requests-Timeout.html
https://stackoverflow.com/questions/35185025/default-keep-alive-time-for-a-httpconnection-when-using-spring-rest-template
https://stackoverflow.com/questions/16256681/how-to-reuse-httpurlconnection
https://stackoverflow.com/questions/40161117/spring-resttemplate-need-to-release-connection
https://howtodoinjava.com/spring-restful/resttemplate-httpclient-java-config/
https://stackoverflow.com/questions/31869193/using-spring-rest-template-either-creating-too-many-connections-or-slow
https://gist.github.com/soverby/0dbeab4d77b402e178eabb66a06407c0
https://dzone.com/articles/caching-with-apache-http-client-and-spring-resttem
