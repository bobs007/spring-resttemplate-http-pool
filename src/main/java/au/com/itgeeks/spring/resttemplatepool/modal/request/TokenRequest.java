package au.com.itgeeks.spring.resttemplatepool.modal.request;

import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Component
//@Data
// @AllArgsConstructor
//@Setter
//@Getter 
//@ToString 
//@EqualsAndHashCode
public class TokenRequest {
  
  private int code;

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }
  
  
}
