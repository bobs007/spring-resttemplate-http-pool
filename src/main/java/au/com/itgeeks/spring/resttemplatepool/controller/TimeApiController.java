package au.com.itgeeks.spring.resttemplatepool.controller;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;
import java.util.StringJoiner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import au.com.itgeeks.spring.resttemplatepool.modal.request.TokenRequest;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/time")
@Slf4j
public class TimeApiController {


  private static final Logger log = LoggerFactory.getLogger(TimeApiController.class);
  
  @GetMapping
  @ResponseBody
  public ResponseEntity<String> getSystemTime(@RequestBody TokenRequest request) {

    final ResponseEntity<String> resp = ResponseEntity.ok().body(LocalDateTime.now().toString());

    final StringJoiner sj = new StringJoiner(",");

    sj.add(resp.getStatusCodeValue() + "");
    sj.add(resp.getBody());
    // resp.getHeaders().getAccessControlRequestHeaders().stream().forEach(myPojo ->
    // sj.add(myPojo.toString()));

    log.debug(sj.toString());

    // resp.getHeaders().entrySet().stream().collect(Collectors.toMap(Entry::getKey, e ->
    // e.getValue()).stream().forEach(value -> sj.add(value)));

    final StringBuffer sb = new StringBuffer();

    resp.getHeaders().entrySet().stream().filter(entry -> entry.getKey() != null).forEach(entry -> {
      sb.append("[");
      sb.append(entry.getKey()).append("=");
      final List<String> headerValues = entry.getValue();
      final Iterator<?> it = headerValues.iterator();
      if (it.hasNext()) {
        sb.append(it.next());
        while (it.hasNext()) {
          sb.append(",").append(it.next());
        }
      }
      sb.append("]");
      log.debug(sb.toString());
    });

    

    return resp;
  }


  @PostMapping
  @ResponseBody
  public ResponseEntity<String> create(@RequestBody final TokenRequest request) {

    log.debug(request.toString());

    final ResponseEntity<String> resp = ResponseEntity.ok().body(LocalDateTime.now().toString());

    final StringJoiner sj = new StringJoiner(",");

    sj.add(resp.getStatusCodeValue() + "");
    sj.add(resp.getBody());
    // resp.getHeaders().getAccessControlRequestHeaders().stream().forEach(myPojo ->
    // sj.add(myPojo.toString()));

    log.debug(sj.toString());

    // resp.getHeaders().entrySet().stream().collect(Collectors.toMap(Entry::getKey, e ->
    // e.getValue()).stream().forEach(value -> sj.add(value)));

    final StringBuffer sb = new StringBuffer().append("[");

    resp.getHeaders().entrySet().stream().filter(entry -> entry.getKey() != null).forEach(entry -> {
      sb.append(entry.getKey()).append("=");
      final List<String> headerValues = entry.getValue();
      final Iterator<?> it = headerValues.iterator();
      if (it.hasNext()) {
        sb.append(it.next());
        while (it.hasNext()) {
          sb.append(",").append(it.next());
        }
      }
      sb.append("]");
    });

    log.debug(sb.toString());

    return resp;
  }

}
