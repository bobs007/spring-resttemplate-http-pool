package au.com.itgeeks.spring.resttemplatepool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ResttemplatePoolApplication {

  public static void main(final String[] args) {
    SpringApplication.run(ResttemplatePoolApplication.class, args);
  }
}
