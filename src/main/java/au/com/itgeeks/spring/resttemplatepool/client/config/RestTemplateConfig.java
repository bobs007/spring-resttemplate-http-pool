package au.com.itgeeks.spring.resttemplatepool.client.config;

import org.apache.http.HttpHost;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import au.com.itgeeks.spring.resttemplatepool.client.config.HttpHostsConfiguration.HttpHostConfiguration;

/**
 * 
 * @author bobs
 *
 */
@Configuration
@EnableAutoConfiguration
@EnableConfigurationProperties(HttpHostsConfiguration.class)
@ComponentScan({"au.com.itgeeks.spring.resttemplatepool"})
public class RestTemplateConfig {

  @Autowired
  private HttpHostsConfiguration httpHostConfiguration;

  @Bean
  @Qualifier("without")
  public RestTemplate restTemplateWithOut() {
    return new RestTemplate();
  }

  @Bean
  @Qualifier("with")
  public RestTemplate restTemplate() {
    return restTemplate(httpClient(poolingHttpClientConnectionManager(), requestConfig()));
  }

  
  private PoolingHttpClientConnectionManager poolingHttpClientConnectionManager() {
    final PoolingHttpClientConnectionManager result = new PoolingHttpClientConnectionManager();
    result.setMaxTotal(httpHostConfiguration.getMaxTotal());
    // Default max per route is used in case it's not set for a specific route
    result.setDefaultMaxPerRoute(httpHostConfiguration.getDefaultMaxPerRoute());
    if (!CollectionUtils.isEmpty(httpHostConfiguration.getMaxPerRoutes())) {
      for (final HttpHostConfiguration httpHostConfig : httpHostConfiguration.getMaxPerRoutes()) {
        final HttpHost host = new HttpHost(httpHostConfig.getHost(), httpHostConfig.getPort(),
            httpHostConfig.getScheme());
        // Max per route for a specific host route
        result.setMaxPerRoute(new HttpRoute(host), httpHostConfig.getMaxPerRoute());
      }
    }
    return result;
  }


  private RequestConfig requestConfig() {
    final RequestConfig result = RequestConfig.custom().setConnectionRequestTimeout(2000)
        .setConnectTimeout(2000).setSocketTimeout(2000).build();
    return result;
  }

  private CloseableHttpClient httpClient(
      final PoolingHttpClientConnectionManager poolingHttpClientConnectionManager,
      final RequestConfig requestConfig) {
    final CloseableHttpClient result =
        HttpClientBuilder.create().setConnectionManager(poolingHttpClientConnectionManager)
            .setDefaultRequestConfig(requestConfig).build();
    return result;
  }
  
  private RestTemplate restTemplate(final HttpClient httpClient) {
    final HttpComponentsClientHttpRequestFactory requestFactory =
        new HttpComponentsClientHttpRequestFactory();
    requestFactory.setHttpClient(httpClient);
    return new RestTemplate(requestFactory);
  }
  
}
