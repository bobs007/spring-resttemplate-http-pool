package au.com.itgeeks.spring.resttemplatepool.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * 
 * @author bobs
 *
 */
@Component
public class RestTemplateClient {

  
  private static final Logger logger = LoggerFactory.getLogger(RestTemplateClient.class); 
  
  @Autowired
  @Qualifier("without")
  private RestTemplate restTemplateWithOut;
  
  @Autowired
  @Qualifier("with")
  private RestTemplate restTemplate;
  
  private static final String url = "http://localhost:8001/time";
  
  /**
   * 
   */
  public void callGetMethodWithOutPool() {
    logger.debug("inside callGetMethodWithOutPool()");
    String response = restTemplateWithOut.getForObject(url, String.class);
    logger.debug("end callGetMethodWithOutPool(), response = {}", response);
  }

  /**
   * 
   */
  public void callGetMethodWithPool() {
    logger.debug("inside callGetMethodWithOutPool()");
    String response =  restTemplate.getForObject(url, String.class);
    logger.debug("end callGetMethodWithOutPool(), response = {}", response);
  }
  
}
