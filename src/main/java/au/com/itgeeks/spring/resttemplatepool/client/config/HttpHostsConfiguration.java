package au.com.itgeeks.spring.resttemplatepool.client.config;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;

@Primary
@Configuration
@PropertySource(value = "classpath:application.yml",factory = YamlPropertyLoaderFactory.class)
@ConfigurationProperties(prefix = "httpconnpool")
public class HttpHostsConfiguration {

  private Integer maxTotal;
  private Integer defaultMaxPerRoute;
  private List<HttpHostConfiguration> maxPerRoutes;

  public Integer getMaxTotal() {
    return maxTotal;
  }

  public void setMaxTotal(final Integer maxTotal) {
    this.maxTotal = maxTotal;
  }

  public Integer getDefaultMaxPerRoute() {
    return defaultMaxPerRoute;
  }

  public void setDefaultMaxPerRoute(final Integer defaultMaxPerRoute) {
    this.defaultMaxPerRoute = defaultMaxPerRoute;
  }

  public List<HttpHostConfiguration> getMaxPerRoutes() {
    return maxPerRoutes;
  }

  public void setMaxPerRoutes(final List<HttpHostConfiguration> maxPerRoutes) {
    this.maxPerRoutes = maxPerRoutes;
  }


  public static class HttpHostConfiguration {

    private String scheme;
    private String host;
    private Integer port;
    private Integer maxPerRoute;

    public String getScheme() {
      return scheme;
    }

    public void setScheme(final String scheme) {
      this.scheme = scheme;
    }

    public String getHost() {
      return host;
    }

    public void setHost(final String host) {
      this.host = host;
    }

    public Integer getPort() {
      return port;
    }

    public void setPort(final Integer port) {
      this.port = port;
    }

    public Integer getMaxPerRoute() {
      return maxPerRoute;
    }

    public void setMaxPerRoute(final Integer maxPerRoute) {
      this.maxPerRoute = maxPerRoute;
    }

  }


}
